class Busary < ApplicationRecord
  belongs_to :student
  belongs_to :institution
  has_one :financial_year
end
