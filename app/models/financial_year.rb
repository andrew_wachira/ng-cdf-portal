class FinancialYear < ApplicationRecord
  has_many :disbursments, dependent: :destroy
  has_many :busaries
  has_many :collections
  has_many :projects
end
