class Project < ApplicationRecord
  belongs_to :ward
  has_many :images
  belongs_to :financial_year
end
