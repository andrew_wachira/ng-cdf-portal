class Student < ApplicationRecord
  has_many :busaries
  belongs_to :institution

  def self.search(search)
    if search
      where('surname LIKE ?', "%#{search}%")
    else
      Student.all
    end
  end

end
