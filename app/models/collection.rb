class Collection < ApplicationRecord
  belongs_to :institution
  belongs_to :financial_year
end
