class Disbursment < ApplicationRecord
  has_one :financial_year

  def self.import_file(file)
    records = CSV.foreach(file.path, headers: true, :encoding => 'ISO-8859-1').map { |col| Hash[institution_name: col[0], student_name: col[1], reg_no: col[2], level: col[3], amount: col[4]
] }
    r = []
    records.each do |rec|
      if rec[:institution_name] != nil && rec[:amount] != nil && rec[:student_name] != nil
        rec[:amount] = rec[:amount].gsub(/[^\d\.]/, '').to_d
        r << rec
      end
    end
    Disbursment.import r
  end


end
