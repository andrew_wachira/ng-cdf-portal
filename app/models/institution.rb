class Institution < ApplicationRecord
  has_many :students
  has_many :busaries
  belongs_to :county
  belongs_to :school_type
  has_many :collections
end
