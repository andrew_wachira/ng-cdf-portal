class Image < ApplicationRecord
  mount_uploader :image, ImageUploader
  belongs_to :project
  belongs_to :constituency
end
