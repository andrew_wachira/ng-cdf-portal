class CollectionsController < ApplicationController
  before_action :set_institution, only: [:create]

  def create
    @collection = @institution.collections.create!(collection_params)
    # Update busary status to true of current f?y
    @institution.busaries.where(financial_year_id: params[:financial_year_id]).update_all(status: true)
    redirect_to institution_path(@institution), notice: 'New Collection'
  end

private

  def collection_params
    params.permit(:name, :contact, :cheque, :amount, :date, :financial_year_id)
  end

  def set_institution
    @institution = Institution.find(params[:institution_id])
  end

end
