class FinancialYearsController < ApplicationController

  def create
    @disbursments = Disbursment.where(financial_year_id: nil)
    if @disbursments.any?
    @batch_busaries = []
      
      # Check Schools that exist and Students that do
      @disbursments.select(:institution_name).map(&:institution_name).uniq.each do |inst|
        if Institution.where(name: inst).exists?
          puts inst + " exists!!, check is students exists and update"
          @disbursments.where(institution_name: inst).each do |student|
            institution = Institution.find_by_name(inst)
            if institution.students.where(first_name: student.student_name.split.first, surname: student.student_name.split.last, reg_no: student.reg_no).exists?
              beneficiary = institution.students.where(first_name: student.student_name.split.first, surname: student.student_name.split.last, reg_no: student.reg_no)
              @batch_busaries << beneficiary[0].busaries.create!(amount: student.amount, institution_id: beneficiary[0].institution_id)
            else
              puts "Student does not exist, create"
              beneficiary = institution.students.create!(first_name: student.student_name.split.first, middle_name: if student.student_name.split.size == 3 then student.student_name.split[1] end, surname: student.student_name.split.last, reg_no: student.reg_no, level: student.level)
              @batch_busaries << beneficiary.busaries.create!(amount: student.amount, institution_id: beneficiary.institution_id)
            end

          end

        else
          puts inst + " does not exist, create it and create students"
          county_id = County.first.id
          institution = Institution.create!(name: inst, county_id: county_id, school_type_id: 4)
          students = []
          students << @disbursments.where(institution_name: inst).select(:student_name, :amount, :reg_no, :level)
          students[0].each do |student|
            beneficiary = institution.students.create!(first_name: student.student_name.split.first, middle_name: if student.student_name.split.size == 3 then student.student_name.split[1] end, surname: student.student_name.split.last, reg_no: student.reg_no, level: student.level)
            @batch_busaries << beneficiary.busaries.create!(amount: student.amount, institution_id: beneficiary.institution_id)
          end
        end
      
    end

      puts @batch_busaries.count

      if FinancialYear.where(f_year: financial_year_params[:f_year]).exists?
        @financial_year = FinancialYear.find_by_f_year(financial_year_params[:f_year])
        @disbursments.update_all(financial_year_id: @exists_financial_year.id)
        @batch_busaries.each do |busary|
          busary.update(financial_year_id: @financial_year.id)
        end
      else
        @financial_year = FinancialYear.create!(financial_year_params)
        @disbursments.update_all(financial_year_id: @financial_year.id)
        @batch_busaries.each do |busary|
          busary.update(financial_year_id: @financial_year.id)
        end
      end

      redirect_to disbursment_busaries_path, notice: 'New Disbursment'
    else
      redirect_to disbursment_busaries_path, notice: 'Upload Fresh Records'
    end
  end


  private

  def financial_year_params
    params.permit(:f_year)
  end

end
