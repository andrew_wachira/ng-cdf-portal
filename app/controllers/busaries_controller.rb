class BusariesController < ApplicationController
  before_action :set_busary, only: [:show, :edit, :update, :destroy, :award]

  # GET /busaries
  # GET /busaries.json

  def import
    Disbursment.import_file(params[:file])
    redirect_to disbursment_busaries_path, notice: "Busaries Imported"
  end

  def disbursment
    @disbursments = Disbursment.where(financial_year_id: nil)
    @financial_years = FinancialYear.all
  end

  def index
    @students = Student.all
    @institutions = Institution.all
    @busaries = Busary.all
    @sum_total = 0
    @busaries.each { |busary| @sum_total += busary.amount  }
  end

  # GET /busaries/1
  # GET /busaries/1.json
  def show
    
  end

  def analytics

  end

  def award
    @busary.update(status: true, issue_date: Date.today)
    @student = @busary.student
    redirect_to @student, notice: 'Busary Awarded'
  end

  # GET /busaries/new
  def new
    @busary = Busary.new
  end

  # GET /busaries/1/edit
  def edit
  end

  # POST /busaries
  # POST /busaries.json
  def create
    @busary = Busary.new(busary_params)
    @student = Student.find(busary_params[:student_id])

    respond_to do |format|
      if @busary.save
        format.html { redirect_to @student, notice: 'Busary was successfully created.' }
        format.json { render :show, status: :created, location: @busary }
      else
        format.html { render :new }
        format.json { render json: @busary.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /busaries/1
  # PATCH/PUT /busaries/1.json
  def update
    respond_to do |format|
      if @busary.update(busary_params)
        format.html { redirect_to @busary, notice: 'Busary was successfully updated.' }
        format.json { render :show, status: :ok, location: @busary }
      else
        format.html { render :edit }
        format.json { render json: @busary.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /busaries/1
  # DELETE /busaries/1.json
  def destroy
    @busary.destroy
    respond_to do |format|
      format.html { redirect_to busaries_url, notice: 'Busary was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_busary
      @busary = Busary.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def busary_params
      params.require(:busary).permit(:amount, :status, :student_id, :institution_id)
    end
end
