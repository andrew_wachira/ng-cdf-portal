class StudentsController < ApplicationController
  before_action :set_student, only: [:show, :edit, :update, :destroy]
  helper_method :sort_column, :sort_direction
  # GET /students
  # GET /students.json
  def index
    @students = Student.search(params[:search]).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
  end

  # GET /students/1
  # GET /students/1.json
  def show
    @busaries = Busary.where(student_id: params[:id]).paginate(:per_page => 5, :page => params[:page])
    @collected = @busaries.where(status: true)
    @sum_collected = 0
    @collected.each do |sum|
      @sum_collected += sum.amount
    end
    # sum total of busaries issued
    @sum_total = 0
    @busaries.each do |sum|
      @sum_total += sum.amount
    end
  end

  # GET /students/new
  def new
    @student = Student.new
  end

  # GET /students/1/edit
  def edit
  end

  # POST /students
  # POST /students.json
  def create
    @student = Student.new(student_params)

    respond_to do |format|
      if @student.save
        format.html { redirect_to @student, notice: 'Student was successfully created.' }
        format.json { render :show, status: :created, location: @student }
      else
        format.html { render :new }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    respond_to do |format|
      if @student.update(student_params)
        format.html { redirect_to @student, notice: 'Student was successfully updated.' }
        format.json { render :show, status: :ok, location: @student }
      else
        format.html { render :edit }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @student.destroy
    respond_to do |format|
      format.html { redirect_to students_url, notice: 'Student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # default sort
    def sort_column
      params[:sort] || "first_name"
    end

    def sort_direction
      params[:direction] || "asc"
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_params
      params.require(:student).permit(:first_name, :middle_name, :surname, :reg_no, :level, :institution_id, :notes)
    end
end
