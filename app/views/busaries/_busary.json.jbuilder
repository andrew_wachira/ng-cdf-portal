json.extract! busary, :id, :amount, :status, :created_at, :updated_at
json.url busary_url(busary, format: :json)
