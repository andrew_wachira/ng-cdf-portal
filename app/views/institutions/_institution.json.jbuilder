json.extract! institution, :id, :name, :type, :county, :address, :phone, :email, :created_at, :updated_at
json.url institution_url(institution, format: :json)
