json.extract! constituency, :id, :name, :member_of_parliament, :created_at, :updated_at
json.url constituency_url(constituency, format: :json)
