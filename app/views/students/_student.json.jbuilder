json.extract! student, :id, :first_name, :middle_name, :surname, :reg_no, :level, :notes, :created_at, :updated_at
json.url student_url(student, format: :json)
