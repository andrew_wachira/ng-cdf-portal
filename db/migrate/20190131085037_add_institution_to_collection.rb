class AddInstitutionToCollection < ActiveRecord::Migration[5.1]
  def change
    add_reference :collections, :institution, foreign_key: true
  end
end
