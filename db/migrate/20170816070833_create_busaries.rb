class CreateBusaries < ActiveRecord::Migration[5.1]
  def change
    create_table :busaries do |t|
      t.decimal :amount, precision: 11, scale: 2
      t.boolean :status, default: false

      t.timestamps
    end
  end
end
