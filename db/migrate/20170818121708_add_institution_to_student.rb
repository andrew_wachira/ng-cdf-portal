class AddInstitutionToStudent < ActiveRecord::Migration[5.1]
  def change
    add_reference :students, :institution, foreign_key: true
  end
end
