class RenameColFinancialYear < ActiveRecord::Migration[5.1]
  def change
    rename_column :financial_years, :financial_year, :f_year
  end
end
