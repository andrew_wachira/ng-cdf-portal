class CreateConstituencies < ActiveRecord::Migration[5.1]
  def change
    create_table :constituencies do |t|
      t.string :name
      t.string :member_of_parliament

      t.timestamps
    end
  end
end
