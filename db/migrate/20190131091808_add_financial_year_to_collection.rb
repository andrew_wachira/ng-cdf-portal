class AddFinancialYearToCollection < ActiveRecord::Migration[5.1]
  def change
    add_reference :collections, :financial_year, foreign_key: true
  end
end
