class AddDateToProject < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :start_date, :date
  end
end
