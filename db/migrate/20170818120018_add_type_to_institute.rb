class AddTypeToInstitute < ActiveRecord::Migration[5.1]
  def change
    add_reference :institutions, :school_type, foreign_key: true
  end
end
