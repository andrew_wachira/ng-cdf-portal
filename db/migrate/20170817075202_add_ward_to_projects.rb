class AddWardToProjects < ActiveRecord::Migration[5.1]
  def change
    add_reference :projects, :ward, foreign_key: true
  end
end
