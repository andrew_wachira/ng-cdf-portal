class CreateDisbursments < ActiveRecord::Migration[5.1]
  def change
    create_table :disbursments do |t|
      t.string :institution_name
      t.string :student_name
      t.string :reg_no
      t.string :level
      t.decimal :amount

      t.timestamps
    end
  end
end
