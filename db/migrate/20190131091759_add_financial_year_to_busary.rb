class AddFinancialYearToBusary < ActiveRecord::Migration[5.1]
  def change
    add_reference :busaries, :financial_year, foreign_key: true
  end
end
