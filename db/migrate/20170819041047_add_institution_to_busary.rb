class AddInstitutionToBusary < ActiveRecord::Migration[5.1]
  def change
    add_reference :busaries, :institution, foreign_key: true
  end
end
