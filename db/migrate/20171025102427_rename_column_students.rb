class RenameColumnStudents < ActiveRecord::Migration[5.1]
  def change
    rename_column :students, :sir_name, :surname
  end
end
