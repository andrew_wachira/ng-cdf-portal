class RemoveTypeFromIntitute < ActiveRecord::Migration[5.1]
  def change
    remove_column :institutions, :type
  end
end
