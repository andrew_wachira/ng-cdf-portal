class AddFinancialYearToDisbursment < ActiveRecord::Migration[5.1]
  def change
    add_reference :disbursments, :financial_year, foreign_key: true
  end
end
