class AddCountyToInstitute < ActiveRecord::Migration[5.1]
  def change
    add_reference :institutions, :county, foreign_key: true
  end
end
