class AddStudentToBusary < ActiveRecord::Migration[5.1]
  def change
    add_reference :busaries, :student, foreign_key: true
  end
end
