class AddFinancialYearToProjects < ActiveRecord::Migration[5.1]
  def change
    add_reference :projects, :financial_year, foreign_key: true
  end
end
