class CreateFinancialYears < ActiveRecord::Migration[5.1]
  def change
    create_table :financial_years do |t|
      t.string :financial_year

      t.timestamps
    end
  end
end
