class AddDescriptionToWard < ActiveRecord::Migration[5.1]
  def change
    add_column :wards, :description, :string
    add_column :wards, :text, :string
  end
end
