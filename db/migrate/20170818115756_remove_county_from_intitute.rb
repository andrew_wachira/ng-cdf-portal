class RemoveCountyFromIntitute < ActiveRecord::Migration[5.1]
  def change
    remove_column :institutions, :county
  end
end
