class RenameColumnImages < ActiveRecord::Migration[5.1]
  def self.up
    rename_column :images, :constituencies_id, :constituency_id
  end

  def self.down

  end
end
