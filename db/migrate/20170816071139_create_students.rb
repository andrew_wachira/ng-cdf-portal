class CreateStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :students do |t|
      t.string :first_name
      t.string :middle_name
      t.string :sir_name
      t.string :reg_no
      t.string :level
      t.text :notes

      t.timestamps
    end
  end
end
