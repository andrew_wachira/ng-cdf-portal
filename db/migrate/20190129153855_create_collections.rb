class CreateCollections < ActiveRecord::Migration[5.1]
  def change
    create_table :collections do |t|
      t.string :name
      t.string :contact
      t.string :cheque
      t.decimal :amount
      t.string :date

      t.timestamps
    end
  end
end
