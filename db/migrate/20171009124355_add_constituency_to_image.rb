class AddConstituencyToImage < ActiveRecord::Migration[5.1]
  def change
    add_reference :images, :constituencies, foreign_key: true
  end
end
