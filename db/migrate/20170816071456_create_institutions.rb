class CreateInstitutions < ActiveRecord::Migration[5.1]
  def change
    create_table :institutions do |t|
      t.string :name
      t.string :type
      t.string :county
      t.string :address
      t.string :phone
      t.string :email

      t.timestamps
    end
  end
end
