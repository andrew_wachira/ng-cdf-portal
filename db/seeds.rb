# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
constituencies = Constituency.create([{ name: 'Juja Constituency' }, { member_of_parliament: 'John Snow' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'csv'

# Seed Counties
County.create!()

csv_text = File.read(Rails.root.join('lib', 'seeds', 'counties.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  t = County.new
  t.name = row['County']
  t.save
  puts "#{t.name}"
end

puts "There are #{County.count} counties in kenya "

SchoolType.create!()
SchoolType.create!(name: 'Boarding')
SchoolType.create!(name: 'Day')
SchoolType.create!(name: 'Mixed')
SchoolType.create!(name: 'University')
