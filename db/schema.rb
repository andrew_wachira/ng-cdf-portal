# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190211132119) do

  create_table "admins", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "busaries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.decimal "amount", precision: 11, scale: 2
    t.boolean "status", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "student_id"
    t.bigint "institution_id"
    t.date "issue_date"
    t.bigint "financial_year_id"
    t.index ["financial_year_id"], name: "index_busaries_on_financial_year_id"
    t.index ["institution_id"], name: "index_busaries_on_institution_id"
    t.index ["student_id"], name: "index_busaries_on_student_id"
  end

  create_table "collections", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.string "contact"
    t.string "cheque"
    t.decimal "amount", precision: 10
    t.string "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "institution_id"
    t.bigint "financial_year_id"
    t.index ["financial_year_id"], name: "index_collections_on_financial_year_id"
    t.index ["institution_id"], name: "index_collections_on_institution_id"
  end

  create_table "constituencies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.string "member_of_parliament"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "about"
  end

  create_table "counties", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "disbursments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "institution_name"
    t.string "student_name"
    t.string "reg_no"
    t.string "level"
    t.decimal "amount", precision: 10
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "financial_year_id"
    t.index ["financial_year_id"], name: "index_disbursments_on_financial_year_id"
  end

  create_table "financial_years", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "f_year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "images", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "image"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "project_id"
    t.bigint "constituency_id"
    t.index ["constituency_id"], name: "index_images_on_constituency_id"
    t.index ["project_id"], name: "index_images_on_project_id"
  end

  create_table "institutions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.string "address"
    t.string "phone"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "school_type_id"
    t.bigint "county_id"
    t.index ["county_id"], name: "index_institutions_on_county_id"
    t.index ["school_type_id"], name: "index_institutions_on_school_type_id"
  end

  create_table "projects", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "ward_id"
    t.date "start_date"
    t.bigint "financial_year_id"
    t.index ["financial_year_id"], name: "index_projects_on_financial_year_id"
    t.index ["ward_id"], name: "index_projects_on_ward_id"
  end

  create_table "school_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "students", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "first_name"
    t.string "middle_name"
    t.string "surname"
    t.string "reg_no"
    t.string "level"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "institution_id"
    t.index ["institution_id"], name: "index_students_on_institution_id"
  end

  create_table "wards", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "description"
    t.string "text"
  end

  add_foreign_key "busaries", "financial_years"
  add_foreign_key "busaries", "institutions"
  add_foreign_key "busaries", "students"
  add_foreign_key "collections", "financial_years"
  add_foreign_key "collections", "institutions"
  add_foreign_key "disbursments", "financial_years"
  add_foreign_key "images", "constituencies"
  add_foreign_key "images", "projects"
  add_foreign_key "institutions", "counties"
  add_foreign_key "institutions", "school_types"
  add_foreign_key "projects", "financial_years"
  add_foreign_key "projects", "wards"
  add_foreign_key "students", "institutions"
end
