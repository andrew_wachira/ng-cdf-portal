Rails.application.routes.draw do
  resources :images
  devise_for :admins
  resources :constituencies
  resources :wards  do
    resources :projects
  end
  resources :institutions do
    resources :collections
  end
  resources :students
  resources :busaries do
    collection do
      post :import
      get :disbursment
    end
  end
  resources :financial_years

  # lannding page
  root 'constituencies#index'

  # custom routes
  get 'analytics', :to => 'busaries#analytics', :as => :analytics
  get 'award/:id', :to => 'busaries#award', :as => :award

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
